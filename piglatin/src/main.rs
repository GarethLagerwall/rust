//Convert strings to pig latin. The first consonant of each word is moved to the end of the word and “ay” is added, so “first” becomes “irst-fay.” Words that start with a vowel have “hay” added to the end instead (“apple” becomes “apple-hay”). Keep in mind the details about UTF-8 encoding!

use std::time::Instant;
use serde::Deserialize;
use std::fs;
use toml;
use unicode_segmentation::UnicodeSegmentation;

#[derive(Debug, Deserialize)]
struct InputToml {
    #[allow(dead_code)] // Disable dead code warning for the entire struct
    stringlist: Vec<String>, //needs same name as found in input file
}


fn main() {
    let now = Instant::now(); //start the timer
    println!("PigLatin! Woo!");
    //parse input file
    let input_str = fs::read_to_string("input.toml").expect("Failed to read Cargo.toml file");
    println!("Input string read from file \n{}",input_str);
    let input_toml: InputToml = toml::from_str(&input_str).expect("Failed to deserialize Cargo.toml");
    println!("parsed input struct \n {:?}", input_toml);

    parsestrings(&input_toml.stringlist);
    
    let elapsed_time = now.elapsed(); //stop timer
    println!("\nCOMPLETED after {} seconds.", elapsed_time.as_secs());
}

fn parsestrings(invec: &Vec<String>){
    let vowels=vec!["a","e","i","o","u"];
    for itstr in invec{
        let sttmp=itstr.to_string();
        println!("string: {}", sttmp);
        // for c in sttmp.chars(){
        //     println!("{c}");
        // }
        let gvec= sttmp.graphemes(true).collect::<Vec<&str>>();
        let mut gvecc = gvec.clone();
        //println!("\tstring as graphemes: {:?}",gvec);
        let firstchar=gvec[0].to_lowercase(); //lowercase to help matching
        //println!("\tfirst character: {}",firstchar);
        if vowels.contains(&firstchar.as_str()){
            //println!("\tItsa vowel");
            gvecc.push("-hay");
            let finalword = gvecc.join("");
            println!("\tTransformed word: {}",finalword)
        }
        else {
            //println!("\t\tItsa consonant");
            //let finalword = gvecc.drain(1..);
            let newstr= format!("-{}ay",firstchar);
            gvecc.push(newstr.as_str());
            let tmpvec = &gvecc[1..] ;
            let finalword = tmpvec.join("");
            //finalword.push(newstr);
            println!("\tTransformed word: {}",finalword);
        }
        //iscons(&itstr);
    }
}

// fn iscons(mystr){
    
// }
