// Given a list of integers, use a vector and return the median (when sorted, the value in the middle position) and mode (the value that occurs most often; a hash map will be helpful here) of the list.
use math::round;
use serde::Deserialize;
use std::fs;
use toml;
use std::time::Instant;
use std::collections::HashMap;


#[derive(Debug, Deserialize)]
struct InputToml {
    #[allow(dead_code)] // Disable dead code warning for the entire struct
    vectors: Vec<Vec<i32>>,
}

fn main() {
    let now = Instant::now(); //start the timer
    println!("Vector stats!");
    //parse input file
    let input_str = fs::read_to_string("input.toml").expect("Failed to read Cargo.toml file");
    println!("Input string read from file \n{}",input_str);
    let input_toml: InputToml = toml::from_str(&input_str).expect("Failed to deserialize Cargo.toml");
    println!("parsed input struct \n {:#?}", input_toml);
    //separate vectors
    let invecs = input_toml.vectors;
    runanalyses(&invecs); //run processing
    let elapsed_time = now.elapsed(); //stop timer
    println!("\nCOMPLETED after {} seconds.", elapsed_time.as_secs());
}

fn runanalyses(invecs :&Vec<Vec<i32>>){
    println!("Running Analysis");
    for (i,vec) in invecs.iter().enumerate(){
        println!("\ncount: {}, vector: {:?}", i, vec);
        if vec.is_empty(){
            println!("\t empty vector, cant compute");
        }
        else if vec.len()==1 {
            println!("\tvector length is 1");
            println!("\tMax, Min, Mean, Mode, Median  = {}", vec[0])
        }
        else {
            println!("\tprocessing stats");
            let mut svec  = vec.clone();
            svec.sort();
            let mymax = svec[svec.len()-1];
            let mymin = svec[0];
            println!("\t\tMax: {} \t Min: {}", mymax, mymin);
            mymedian(&svec);
            mymean(&svec);
            mymode(&svec);
        }
    }
}

fn mymean(v :&Vec<i32>) {
    let mut sum = 0;
    for i in v.iter(){
        sum+=i;
    }
    //println!("\t\tSum: {} \t length: {}",sum,v.len());
    let vmean = sum/(v.len() as i32);
    print!("\t\tMean: {}",vmean);
}

fn mymode(v :&Vec<i32>) {
    let mut bins = HashMap::new();
    for vit in v.iter(){
        let count = bins.entry(vit).or_insert(0);
        *count += 1
    }
    //println!("\n\t\tBins and counts {:?}",bins);
    let maxcount=bins.values().max().expect("error");
    //println!("\t\tmax count: {}",maxcount);
    for (k,v) in bins.iter(){
        //println!("key {}, value {}",k,v);
        if v==maxcount {
            println!("\n\t\tMode: {}", k)
        }
    }
}

fn mymedian(v :&Vec<i32>) {
    //println!("Finding median");
    //println!("passed vector: {:?}",v);
    let veclen =v.len();
    //println!("vec len {}",veclen);
    //println!("{}",isodd(&veclen));
    if isodd(&veclen) {
        let iterloc = round::floor((&veclen/2) as f64,0) as usize;
        //println!("iter location: {}",iterloc);
        println!("\t\tMedian: {}",v[iterloc]);
        // mymedian=v[]
    } else {
        let itloc1= round::floor((&veclen/2) as f64,0) as usize;
        let itloc2 = itloc1+1;
        let vecp1=v[itloc1];
        let vecp2=v[itloc2];
        let mymedian=(vecp1+vecp2)/2;
        println!("\t\tMedian: {}",mymedian);
    }
}

fn isodd(n :&usize) -> bool {
    //assert_eq!(n%2, 0);
    if n%2==0 {
        //println!("even length");
        false
    } else {
        //println!("odd length");
        true
    }
}
