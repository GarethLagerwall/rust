// Using a hash map and vectors, create a text interface to allow a user to add employee names to a department in a company. For example, “Add Sally to Engineering” or “Add Amir to Sales.” Then let the user retrieve a list of all people in a department or all people in the company by department, sorted alphabetically.
use std::time::Instant;
use std::io;
use std::collections::HashMap;
use itertools::Itertools;
    
fn main() {
    let now = Instant::now(); //start the timer
    println!("Interactive binary");
    let mut dbase: HashMap<String,Vec<String>> = HashMap::new();
    let mut testdb: HashMap<String,Vec<String>> = HashMap::from([
        ("b".to_string(),vec!["c".to_string(),"d".to_string(),"x".to_string()]),
        ("a".to_string(),vec!["m".to_string(),"e".to_string(),"y".to_string()]),
        ("g".to_string(),vec!["x".to_string(),"j".to_string(),"g".to_string()]),
    ]);
    mainmenu(&mut dbase);

    //mainmenu(&mut testdb);
    //reviewdb(&mut testdb);
    let elapsed_time = now.elapsed(); //stop timer
    println!("\nCOMPLETED after {} seconds.", elapsed_time.as_secs());
}

fn mainmenu(db: &mut HashMap<String,Vec<String>>) {
    loop {
        println!("\nMain Menu");
        println!("press '1' for adding person\n\
                  press '2' for reviewing database\n\
                  press '3' to exit");
        let mut mainin = String::new();
        io::stdin()
            .read_line(&mut mainin)
            .expect("Failed to read line");
        //let mainin: u32 = mainin.trim().parse().expect("Please type a number!");
        let mainin: u32 = match mainin.trim().parse() {
            Ok(num) => num,
            Err(_) => continue,
        };
        println!("You selected: {mainin}");
        if mainin == 1 {
            println!("continuing to add person...");
            addperson(db);
        }
        else if mainin ==2 {
            println!("continuing to review database...");
            reviewdb(db);
        }
        else if mainin ==3 {
            println!("exiting...");
            return;
        }
        else {
            println!("please select a number from the options listed")
        }
    }
}

fn addperson(db: &mut HashMap<String,Vec<String>>) {
    loop {
        println!("Adding person Menu");

        println!("press 1 to add preson\n\
                  press 2 to return to main menu");        
        let mut personchoice = String::new();
        io::stdin()
            .read_line(&mut personchoice)
            .expect("Failed to read line");
        //let personchoice: u32 = personchoice.trim().parse().expect("Please type a number!");
        let personchoice: u32 = match personchoice.trim().parse() {
            Ok(num) => num,
            Err(_) => continue,
        };
        println!("You entered: {personchoice}");
        
        if personchoice == 1 {
            println!("Please enter the person, followed by a comma and their department\ne.g. john doe, accounting");
            let mut personin = String::new();
            io::stdin()
                .read_line(&mut personin)
                .expect("Failed to read line");
            //let personin: u32 = personin.trim().parse().expect("Please type a number!");
            let personin  = match personin.trim().parse::<String>() {
                Ok(entry) => entry,
                Err(_) => continue,
            };
            //let personin = personin.to_owned();
            println!("You entered: {}", personin);
            let linein = personin.trim().split(",");
            let inwords: Vec<&str> = linein.collect();
            let person = inwords[0].to_string();
            let dept = inwords[1].to_string();
            //personin.clone_from(&personin);
            // let vecnames = personin.split(",").map(|s| s.to_owned()).collect::<Vec<_>>();//.split(",").collect();
            // //let mut vecnames: Vec<&str> = vecnames.split(",").collect();
            // //let vecnames: Vec<&str> = vecnames.collect();
            // if vecnames.len()!=2 {
            //     println!("need a comma separated name and dept");
            //     continue;
            // }
            // let person  = vecnames[0].trim().collect();//.to_owned();
            // let dept = vecnames[1].trim().to_owned();
            println!("parsed as preson: {} and dept: {}",person,dept);

            println!("press 1 to confirm add\n\
                      press any other number to return to person menu");        
            let mut personconfirm = String::new();
            io::stdin()
                .read_line(&mut personconfirm)
                .expect("Failed to read line");
            //let personconfirm: u32 = personconfirm.trim().parse().expect("Please type a number!");
            let personconfirm: u32 = match personconfirm.trim().parse() {
                Ok(num) => num,
                Err(_) => continue,
            };
            println!("You entered: {personconfirm}");
            if personconfirm == 1 {
                println!("confirmed add person");
                db.entry(dept).or_insert_with(Vec::new).push(person);
                println!("database {:?}",db);
            }
            else {continue;}
            
        }
        else if personchoice == 2 {
            println!("exiting to main menu...");
            return;
        }
        else {
            println!("please select a number from the options listed")
        }

    }
}

fn reviewdb(db: &mut HashMap<String,Vec<String>>){
    loop {
        println!("\nReviewing Database");
        println!("press '1' for listing people in a department\n\
                  press '2' for listing all people in the database\n\
                  press '3' to exit");
        let mut revin = String::new();
        io::stdin()
            .read_line(&mut revin)
            .expect("Failed to read line");
        //let revin: u32 = revin.trim().parse().expect("Please type a number!");
        let revin: u32 = match revin.trim().parse() {
            Ok(num) => num,
            Err(_) => continue,
        };
        println!("You selected: {revin}");
        if revin == 1 {
            println!("Reviewing department...");
            println!("departments are: {:?}",db.keys());
            println!("enter the depeartment of interest");
            let mut deptin = String::new();
            io::stdin()
                .read_line(&mut deptin)
                .expect("Failed to read line");
            //let personin: u32 = personin.trim().parse().expect("Please type a number!");
            let deptin  = match deptin.trim().parse::<String>() {
                Ok(entry) => entry,
                Err(_) => continue,
            };
            //let personin = personin.to_owned();
            println!("You entered: {}", deptin);
            let dkeys: Vec<String> = db.keys().cloned().collect();
            if dkeys.contains(&deptin){
                println!("department exists");
                println!("Employees are:");
                let mut emplist = db.get(&deptin).unwrap().to_owned();
                emplist.sort();
                println!("{:?}",emplist)
            }
            else {
                println!("department doesn't exist");
            }
        }
        else if revin == 2 {
            println!("listing all people in the database...");
            println!("{:?}",db);
            for dept in db.keys().sorted() {
                println!("Department: {}",dept);
                // let emplist: &Vec<String> = match db.get(dept){
                //     Ok(list) => list,
                //     Err(_) => println!("error retrieving employees"),
                // };
                let mut emplist = db.get(dept).unwrap().to_owned();
                //let employsrt = emplist.sort();//std::sort(&emplist);
                emplist.sort();
                println!("\temployees: {:?}",emplist);
                // for emp in db.get_key_value(dept).sorted(){
                //     println!("\t Employee: {}",emp)
                // }
            }
            // let mut dbkeys: Vec<&String> = db.keys().sorted();
            // let dbkeys = dbkeys.sort();
            // println!("{:?}",dbkeys)
        }
        else if revin == 3 {
            println!("exiting to main menu...");
            return;
        }
        else {
            println!("please select a number from the options listed")
        }
    }

}
