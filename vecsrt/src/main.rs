fn main() {
    println!("Hello, world!");
    let mut myv = vec!["hello", "world", "how", "are", "you"];
    //let mut myv = vec![2,6,1,5];
    println!("vector is {:?}", myv);
    let mut srtvec = myv.clone();
    srtvec.sort();
    println!("sorted vector is {:?}", srtvec);
    myv.sort();
    println!("sorted vector is {:?}", myv);
}
