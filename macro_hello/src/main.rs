use macro_hello::HelloMacro;
use hello_macro_derive::HelloMacro;

#[derive(HelloMacro)]
struct Pancakes;

#[derive(HelloMacro)]
struct Bloat;


fn main() {
    Pancakes::hello_macro();
    Bloat::hello_macro();
}
