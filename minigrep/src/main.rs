//emacs use M-x eval expression: (rustic-cargo-run-command "-- needle haystack")
//(rustic-cargo-run-command "-- needle poem.txt")
//(rustic-cargo-run-command "-- frog poem.txt")
//(rustic-cargo-run-command "-- body poem.txt")
//(rustic-cargo-run-command "-- monomorphization poem.txt")
//(rustic-cargo-run-command "-- to poem.txt")
//in eshell: (setenv "IGNORE_CASE" "1")
//This will make IGNORE_CASE persist for the remainder of your shell session.
//then: rustic-cargo-run-command "-- to poem.txt"
//to add arguments at runtime
//(rustic-cargo-run-command "-- to poem.txt > output.txt")

use std::env;
use std::process;
use minigrep::Config;
    

fn main() {
    //let args: Vec<String> = env::args().collect();

    let config = Config::build(env::args()).unwrap_or_else(|err|{
        eprintln!("Problem parsing arguments: {err}");
        process::exit(1);
    });
    
    //println!("Searching for: {}", config.query);
    //println!("In file: {}", config.file_path);

    if let Err(e) = minigrep::run(config) {
        eprintln!("Application error: {e}");
        process::exit(1);
    }
}

